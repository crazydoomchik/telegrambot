﻿
using System;
using Telegram.Bot;
using Xunit;

namespace Task11.Tests
{
    
    public class TelegramBotTests
    {
        [Theory]
        [InlineData("1234567:4TT8bAc8GHUspu3ERYn-KGcvsvGB9u_n4ddy", 1234567)]
        [InlineData("9:jdsaghdfilghdfiugherh", 9)]
        [InlineData("0:foo", 0)]
        [InlineData("5:", 5)]
        [InlineData("-123::::", -123)]
        public void Should_Parse_Bot_Id(string token, int expectedId)
        {
            ITelegramBotClient botClient = new TelegramBotClient(token);
            Assert.Equal(expectedId, botClient.BotId);
        }

        [Theory]
        [InlineData("")]
        [InlineData(":")]
        [InlineData("1234567")]
        [InlineData("INVALID:4TT8bAc8GHUspu3ERYn-KGcvsvGB9u_n4ddy")]
        public void Should_Throw_On_Invalid_Token(string invalidToken)
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => new TelegramBotClient(invalidToken)
            );
            Assert.Equal("token", exception.ParamName);
            
        }
    }
}