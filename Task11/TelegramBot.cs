﻿using BankApi;
using BankApi.Interface;
using LocalizationCultureCore.StringLocalizer;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Ninject;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Task11.Interface;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;


[assembly:InternalsVisibleTo("Task11Tests")]
namespace Task11
{   

    public class TelegramBot: ITelegramBot
    {
        private static string BOT_TOKEN = ConfigurationManager.AppSettings["BotToken"];
        private TelegramBotClient _botClient = new TelegramBotClient(BOT_TOKEN);
        private static IKernel _kernel = new StandardKernel(new DIModule());
        private IBankResponse _bankResponse = _kernel.Get<IBankResponse>();
        private static ILogger logger = new Microsoft.Extensions.Logging.Console.ConsoleLogger("", null, false);
        private IStringLocalizer _localizer;

        public void StartBot()
        {
            _botClient.OnMessage += On_Message;
            _botClient.OnMessageEdited += On_Message;
            _botClient.OnCallbackQuery += BotOnCallbackQueryReceived;
            SetCulture();
            _botClient.StartReceiving();
            
            FirstMessage();
        }
         private void SetCulture(string culture = "ru-RU")
        {
            string currentDir = AppDomain.CurrentDomain.BaseDirectory;
            string folderName = "Resources";
            string path = Path.GetFullPath(Path.Combine(currentDir + folderName));
            _localizer = new JsonStringLocalizer(path, "Taks11", logger, CultureInfo.CurrentCulture = new CultureInfo(culture));
        }
        internal async void On_Message(object sender, MessageEventArgs e)
        { 
            
            if (e.Message.Text=="Русский")
            {
                SetCulture();
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("SetDate"));
                return;               
            }
            if(e.Message.Text == "English")
            {
                SetCulture("en-US");
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("SetDate"));
                return;
            }

            if (e.Message.Text.Length == 10)
            {
                SetDate(e);

            }
            else if (e.Message.Text.Length == 3)
            {
                ExchangeRate(e);
                
            }
            else { await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("SetDate")); }

        }
        private async void FirstMessage()
        {
            var key = new ReplyKeyboardMarkup();
            var keybord = new KeyboardButton("hi");
            var inlineKeybord = new InlineKeyboardMarkup(
                InlineKeyboardButton.WithCallbackData("Старт"));

            key.Keyboard = new KeyboardButton[][]
                {
                    new KeyboardButton[]
                    {
                        new KeyboardButton("Русский"),
                        new KeyboardButton("English")
                    },

                };
            await _botClient.SendTextMessageAsync(
                    chatId: 442329319,
                    "Hi",
                    replyMarkup: key
                    );
        }

        private async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await _botClient.SendTextMessageAsync(
                callbackQuery.Message.Chat.Id,
                _localizer.GetString("Setculture"));
        }

        private async void ExchangeRate(MessageEventArgs e)
        {
            if (_bankResponse == null || _bankResponse.ExchangeRate == null)
            {
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("SetDate"));
                return;
            }

            var exchange = _bankResponse.GetCurrencyExchange(e.Message.Text.ToUpper());
            if (exchange == null)
            {
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("IncorrectСurrency"));
                return;
            }

            await _botClient.SendTextMessageAsync(
            chatId: e.Message.Chat,
            text: $"{_localizer.GetString("RequestDate")}{_bankResponse.Date}: \n{_localizer.GetString("Purchase")} {exchange.purchaseRate} \n{_localizer.GetString("Sale")} {exchange.saleRate}"
            );
        }

        private async void SetDate(MessageEventArgs e)
        {
            
            DateTime dDate;
            if (!DateTime.TryParseExact(e.Message.Text, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dDate))
            {
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("IncorrectDate"));
                return;
            }

            _bankResponse.GetValue(dDate);

            if (_bankResponse.ExchangeRate.Count == 0)
            {
                await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("NoData"));
                return;
            }
            await _botClient.SendTextMessageAsync(e.Message.Chat, _localizer.GetString("Setcurrency"));
        }

    }

}
