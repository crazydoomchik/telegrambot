﻿using BankApi;
using BankApi.Interface;
using Ninject.Modules;
using Task11.Interface;

namespace Task11
{
    public class DIModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IBankResponse>().To<BankResponse>();
            Bind<ITelegramBot>().To<TelegramBot>();
        }
    }
}
