﻿using Ninject;
using System;
using System.Globalization;
using Task11.Interface;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernal = new StandardKernel(new DIModule());
            var telegramBot = kernal.Get<ITelegramBot>();
            telegramBot.StartBot();

            Console.WriteLine("Telegram bot started");

            Console.WriteLine("Press any key for exit");

            Console.ReadKey();
            Environment.Exit(1);

        }
    }
}
