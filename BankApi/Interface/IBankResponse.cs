﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankApi.Interface
{
    public interface IBankResponse
    {
        public string Date { get; set; }
        public string Bank { get; set; }
        public double BaseCurrency { get; set; }
        public string BaseCurrencyLit { get; set; }
        public List<ExchangeRate> ExchangeRate { get; set; }

        public void GetValue(DateTime dateTime);
        public ExchangeRate GetCurrencyExchange(string currency);

    }
}
