﻿using BankApi.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

namespace BankApi
{
    public class BankResponse: IBankResponse
    {
        public string Date { get; set; }
        public string Bank { get; set; }
        public double BaseCurrency { get; set; }
        public string BaseCurrencyLit { get; set; }
        public List<ExchangeRate> ExchangeRate { get; set; }

        public void GetValue(DateTime dateTime)
        {
            string date = dateTime.ToString("dd.MM.yyyy");

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ApiUrl"]+date);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "Get";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();

                CopyResponse(JsonConvert.DeserializeObject<BankResponse>(result));
                
            }
        }
        
        private void CopyResponse(BankResponse response)
        {
            Date = response.Date;
            Bank = response.Bank;
            BaseCurrency = response.BaseCurrency;
            BaseCurrencyLit = response.BaseCurrencyLit;
            ExchangeRate = response.ExchangeRate;
        }

        public ExchangeRate GetCurrencyExchange(string currency)
        {
            return ExchangeRate.Where(e => e.currency == currency).FirstOrDefault();
        }
    }

}
