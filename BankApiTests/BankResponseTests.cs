﻿
using BankApi;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

using System.Configuration;
using NUnit.Framework;

namespace BankApiTests
{
    public class BankResponseTests
    {
        [SetUp]
        public void SetUp()
        {
            ConfigurationManager.AppSettings.Set("ApiUrl", "https://api.privatbank.ua/p24api/exchange_rates?json&date=");

        }

        [Test]
        [TestCase("19.04.2020")]
        [TestCase("22.08.2020")]
        public void GetValue(string date)
        {
            DateTime dDate;
            DateTime.TryParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dDate);

            BankResponse bank = new BankResponse();

            bank.GetValue(dDate);

            Assert.NotNull(bank.ExchangeRate);
            Assert.AreEqual(bank.Date, date);
        }

        
    }
}
