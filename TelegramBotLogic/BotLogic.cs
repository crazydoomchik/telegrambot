﻿using BankApi.Interface;
using Castle.Core.Logging;
using Microsoft.Extensions.Localization;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Telegram.Bot;

namespace TelegramBotLogic
{
    public class BotLogic
    {
        private static string BOT_TOKEN = ConfigurationManager.AppSettings["BotToken"];
        private TelegramBotClient _botClient = new TelegramBotClient(BOT_TOKEN);
        private static IKernel _kernel = new StandardKernel(new DIModule());
        private IBankResponse _bankResponse = _kernel.Get<IBankResponse>();
        private static ILogger logger = new Microsoft.Extensions.Logging.Console.ConsoleLogger("", null, false);
        private IStringLocalizer _localizer;
    }
}
